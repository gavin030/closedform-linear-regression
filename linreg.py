"""
==========================================
Closed-form solution for linear regression
==========================================

Author: Haotian Shi, 2017

"""

import numpy as np
from numpy import linalg as LA
import matplotlib.pyplot as plt


class LinearRegression:

    def __init__(self, init_theta=None, alpha=0.01, n_iter=100):
        '''
        Constructor
        '''
        self.alpha = alpha
        self.n_iter = n_iter
        self.theta = init_theta
        self.JHist = None
    

    def gradientDescent(self, X, y, theta):
        '''
        Fits the model via gradient descent
        Arguments:
            X is a n-by-d numpy matrix
            y is an n-dimensional numpy vector
            theta is a d-dimensional numpy vector
        Returns:
            the final theta found by gradient descent
        '''
        n,d = X.shape
        self.JHist = []
        
        for i in xrange(self.n_iter):
            cost = self.computeCost(X, y, theta)
            if i >= 1 and cost > self.JHist[i-1][0]: return self.JHist[i-1][1]
            self.JHist.append( (cost, theta.copy()) )
            print "Iteration: ", i+1, " Cost: ", self.JHist[i][0], " Theta: ", theta            
            theta = theta - self.alpha / n * np.dot(X.T, (self.predict(X) - y))
            self.theta = theta

        return theta
    

    def computeCost(self, X, y, theta):
        '''
        Computes the objective function
        Arguments:
          X is a n-by-d numpy matrix
          y is an n-dimensional numpy vector
          theta is a d-dimensional numpy vector
        Returns:
          a scalar value of the cost  
              ** make certain you don't return a matrix with just one value! **
        '''
        
        n,d = X.shape
        
        cost = 1./(2 * n) * float(np.dot((np.dot(X, theta) - y).T, np.dot(X, theta) - y)[0][0])
        
        return cost
    

    def fit(self, X, y):
        '''
        Trains the model
        Arguments:
            X is a n-by-d numpy matrix
            y is an n-dimensional numpy vector
        '''
        print X
        print X.shape
        n = len(y)
        n,d = X.shape
        if self.theta is None:
            self.theta = np.matrix(np.zeros((d,1)))
        self.theta = self.gradientDescent(X,y,self.theta)    


    def predict(self, X):
        '''
        Used the model to predict values for each instance in X
        Arguments:
            X is a n-by-d numpy matrix
        Returns:
            an n-dimensional numpy vector of the predictions
        '''
        return np.dot(X, self.theta)

        
